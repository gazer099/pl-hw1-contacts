contactList = []


def add(name, phone, nid, email):
    contact = {'name': name, 'phone': phone, 'nid': nid, 'email': email}
    contactList.append(contact)


def get_foundIndexList(key, value):
    foundIndex = 0
    foundIndexList = []
    for i in contactList:
        if i[key] == value:
            foundIndexList.append(foundIndex)
        foundIndex += 1
    return foundIndexList


def show_search(key, value):
    foundIndexList = get_foundIndexList(key, value)
    print(len(foundIndexList), 'data be found')
    for i in foundIndexList:
        print('[Name: {dic[name]:<7s}, Phone: {dic[phone]:<10s}, Nid: {dic[nid]:<8s}, Email: {dic[email]:<16s}]'.format(dic=contactList[i]))


# menu function 1
def input_keyboard():
    name = input('Please input name  : ')
    phone = input('Please input phone : ')
    nid = input('Please input nid   : ')
    email = input('please input email : ')

    attributeList = ['name', 'phone', 'nid', 'email']
    attributeValueList = [name, phone, nid, email]
    for i in range(0, 4):
        if len(get_foundIndexList(attributeList[i], attributeValueList[i])) > 0:
            print(len(get_foundIndexList(attributeList[i], attributeValueList[i])), 'data of', attributeList[i], 'is Repeated')
            check = input('Are you sure add this Data? (Y/N) :')
            while check != 'y' and check != 'Y' and check != 'n' and check != 'N':
                check = input('input error, please input again : ')
            if check == 'y' or check == 'Y':
                add(name, phone, nid, email)
                print('Data add successfully')
                return
            elif check == 'n' or check == 'N':
                print('OK, Data will not be add.')
                return
    add(name, phone, nid, email)
    print('Data add successfully')


# menu function 2
def input_file():
    fileName = input('Please input name of input file (default = contacts_input.txt) : ')
    if fileName == '':
        fileName = 'contacts_input.txt'
    if fileName[-4:] != '.txt':
        fileName += '.txt'
    try:
        fin = open(fileName)
    except:
        print('File not found.')
        return
    while True:
        line = fin.readline()
        if not line:
            print('Input complete.')
            fin.close()
            break
        line = line.split()
        add(line[0], line[1], line[2], line[3])


def output_console(key, reverse):
    outputList = sorted(contactList, key=lambda x: x[key], reverse=reverse)
    for i in outputList:
        print('[Name: {dic[name]:<7s}, Phone: {dic[phone]:<10s}, Nid: {dic[nid]:<8s}, Email: {dic[email]:<16s}]'.format(dic=i))


def output_file(key, reverse, fileName):
    outputList = sorted(contactList, key=lambda x: x[key], reverse=reverse)
    fout = open(fileName, 'w')
    for i in outputList:
        fout.write('[Name: {dic[name]:<7s}, Phone: {dic[phone]:<10s}, Nid: {dic[nid]:<8s}, Email: {dic[email]:<16s}]\n'.format(dic=i))
    fout.close()


def modify_contact(key, value, replaceValue):
    if len(get_foundIndexList(key, value)) == 0:
        print('Data not found.')
    elif len(get_foundIndexList(key, value)) == 1:
        show_search(key, value)
        toModIndexList = get_foundIndexList(key, value)
        print('modify', key, value, '>>>', replaceValue)
        contactList[toModIndexList[0]][key] = replaceValue
        print('Data is modified.')
    elif len(get_foundIndexList(key, value)) > 1:
        show_search(key, value)
        toModIndexList = get_foundIndexList(key, value)
        while True:
            try:
                choose = int(input('Which number of line you want to modify? : '))
            except:
                print('input error, please input again!!')
            else:
                break
        print('modify', key, value, '>>>', replaceValue)
        contactList[toModIndexList[choose-1]][key] = replaceValue
        print('Data is modified.')


def del_contact(key, value):
    if len(get_foundIndexList(key, value)) == 0:
        print('Data not found.')
    elif len(get_foundIndexList(key, value)) == 1:
        show_search(key, value)
        toDelIndexList = get_foundIndexList(key, value)
        del contactList[toDelIndexList[0]]
        print('Data is deleted.')
    elif len(get_foundIndexList(key, value)) > 1:
        show_search(key, value)
        toDelIndexList = get_foundIndexList(key, value)
        while True:
            try:
                choose = int(input('Which number of line you want to modify? : '))
            except:
                print('input error, please input again!!')
            else:
                break
        del contactList[toDelIndexList[choose-1]]
        print('Data is deleted.')


def get_sort_by():
    sortBy = input('Sort by? [name|phone|nid|email] (default = name) : ')
    if sortBy == '':
        sortBy = 'name'
    while sortBy != 'name' and sortBy != 'phone' and sortBy != 'nid' and sortBy != 'email':
        sortBy = input('input error, please input again : ')
        if sortBy == '':
            sortBy = 'name'

    isReverse = input('Need reverse? (Y/N) (default = N) : ')
    if isReverse == '':
        isReverse = 'n'
    while isReverse != 'y' and isReverse != 'Y' and isReverse != 'n' and isReverse != 'N':
        isReverse = input('input error, please input again : ')
        if isReverse == '':
            isReverse = 'n'
    if isReverse == 'y' or isReverse == 'Y':
        isReverse = True
    elif isReverse == 'n' or isReverse == 'N':
        isReverse = False

    return sortBy, isReverse
