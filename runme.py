import os
from operation_module import *


def main():

    while True:
        print('---Welcome to use contacts---')
        print('1) Add contact from keyboard')
        print('2) Add contact from file')
        print('3) Search contacts')
        print('4) Modify contact')
        print('5) Delete contact')
        print('6) Show contacts on console')
        print('7) Output contacts as a file')
        print('0) Exit\n')
        option = input('>>>Please select your option : ')
        if option == '1':
            input_keyboard()
            back()
        elif option == '2':
            input_file()
            back()
        elif option == '3':
            set_data_of_search()
            back()
        elif option == '4':
            do_modify()
            back()
        elif option == '5':
            do_delete()
            back()
        elif option == '6':
            sortBy, isReverse = get_sort_by()
            output_console(sortBy, isReverse)
            back()
        elif option == '7':
            set_output_file()
            back()
        elif option == '0':
            break
        else:
            input('input error, please input again. ')
            os.system('cls')


# menu function 3
def set_data_of_search():
    searchKey = input('Search key by? [name|phone|nid|email] (default = name) : ')
    if searchKey == '':
        searchKey = 'name'
    while searchKey != 'name' and searchKey != 'phone' and searchKey != 'nid' and searchKey != 'email':
        searchKey = input('input error, please input again : ')
        if searchKey == '':
            searchKey = 'name'

    value = input('Please input search data : ')
    show_search(searchKey, value)


# menu function 4
def do_modify():
    modKey = input('Modify key by? [name|phone|nid|email] : ')
    while modKey != 'name' and modKey != 'phone' and modKey != 'nid' and modKey != 'email':
        modKey = input('input error, please input again : ')

    value = input('Please input be modify data : ')
    replaceValue = input('Modify to? : ')
    modify_contact(modKey, value, replaceValue)


# menu function 5
def do_delete():
    delKey = input('Delete key by? [name|phone|nid|email] : ')
    while delKey != 'name' and delKey != 'phone' and delKey != 'nid' and delKey != 'email':
        delKey = input('input error, please input again : ')

    value = input('Please input delete data : ')
    del_contact(delKey, value)


# menu function 7
def set_output_file():
    fileName = input('Please input name of output file (default = contacts_output.txt) : ')
    if fileName == '':
        fileName = 'contacts_output.txt'
    if fileName[-4:] != '.txt':
        fileName += '.txt'
    sortBy, isReverse = get_sort_by()
    output_file(sortBy, isReverse, fileName)
    print('File is generated.')


def back():
    input('\nENTER any key to continue...')
    os.system('cls')

if __name__ == '__main__':
    main()
